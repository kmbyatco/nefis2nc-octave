#!/bin/bash

#for filename in $(find /basin/prog/connectivity/nefis/DAVAO_Delft_outputs/ -type f -name 'trim-DAVAO*.dat');
#for filename in $(find /basin/prog/connectivity/nefis/LANUZA_CoarseGrid_Delft_outputs/2010/ -type f -iname 'trim-*.dat');
#for filename in $(find /basin/prog/connectivity/nefis/VIP_Delft_outputs  -type f -iname 'trim-*.dat');
for filename in $(find /basin/rmt/okhotsk/scratch/connectivity/nefis/TanonStrait_1.5KMgrid_jan232017/ -type f -name "trim*.dat");
do
	echo "Currently coverting " $filename
	if [ -f  "${filename%.dat}.nc" ]; then
		echo ${filename%.dat}.nc already exists
		echo ""
	else
		echo Writing log  $(basename ${filename%.dat}.log )
       		octave --eval "vs_trim2nc( '$filename',  'Format', '64bit', 'epsg', 32651, 'timezone', '+08:00' )" > $(basename ${filename%.dat}.log )  2>&1
	fi
done
